#![feature(int_abs_diff)]
use std::cmp::{Ordering, Reverse};
use std::collections::{BinaryHeap, HashMap};
use std::env;
use std::fs;
use std::hash::Hash;
use std::process;

#[derive(Debug, Clone, Hash, Eq, PartialEq)]
struct Coord {
    x: u32,
    y: u32,
}

impl Coord {
    fn new(x: u32, y: u32) -> Coord {
        Coord { x, y }
    }
}

#[derive(Clone, Eq)]
struct Node<'a> {
    coord: &'a Coord,
    f: u32,
    g: u32,
    h: Option<u32>,
}

impl<'a> Node<'_> {
    fn new(coord: &'a Coord) -> Node {
        Node {
            coord,
            f: u32::MAX,
            g: u32::MAX,
            h: None,
        }
    }
}

// sort nodes by f
impl PartialEq for Node<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.f == other.f
    }
}

impl PartialOrd for Node<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Node<'_> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.f.cmp(&other.f)
    }
}

fn main() {
    let usage = || {
        eprintln!("Usage: a-star maze.txt");
        process::exit(1);
    };

    let mut args = env::args();
    args.next();

    // read given file by filename
    let file = fs::read_to_string(args.next().unwrap_or_else(usage)).unwrap();

    // only one argument should be supplied
    if args.next().is_some() {
        usage();
    }

    let mut coords = Vec::new();
    let mut start = None;
    let mut goal = None;

    for (y, line) in file.lines().enumerate() {
        for (x, char) in line.chars().enumerate() {
            match char {
                '.' => coords.push(Coord::new(x as u32, y as u32)),
                'S' => {
                    coords.push(Coord::new(x as u32, y as u32));
                    start = Some(coords.len() - 1);
                }
                'G' => {
                    coords.push(Coord::new(x as u32, y as u32));
                    goal = Some(coords.len() - 1);
                }
                '#' => (),
                c => panic!("Unexpected character {:?}", c),
            }
        }
    }

    let goal = Node::new(&coords[goal.unwrap()]);
    let mut start = Node::new(&coords[start.unwrap()]);
    start.h = Some(h(start.coord, goal.coord));
    start.f = start.h.unwrap();
    start.g = 0;

    match a_star(&coords, start, goal) {
        None => {
            println!("No path was found.");
            process::exit(2);
        }
        Some(path) => {
            let width = file.lines().next().unwrap().len();
            let mut file = file.into_bytes();
            for coord in path {
                let index = coord.y as usize * (width + 1) + coord.x as usize;
                file[index] = b'x';
            }
            let file: String = file.iter().map(|b| *b as char).collect();
            println!("{}", file);
        }
    }
}

fn d(a: &Coord, b: &Coord) -> u32 {
    if a.x != b.x && a.y != b.y {
        14
    } else {
        10
    }
}


fn h(node: &Coord, goal: &Coord) -> u32 {
    // let x_dist = node.x.abs_diff(goal.x);
    // let y_dist = node.y.abs_diff(goal.y);
    // (x_dist * y_dist) * 10
    let x_dist = node.x.abs_diff(goal.x);
    let y_dist = node.y.abs_diff(goal.y);
    ((x_dist * x_dist + y_dist * y_dist) as f32).sqrt().abs() as u32 * 10
}

fn a_star(coords: &[Coord], start: Node, goal: Node) -> Option<Vec<Coord>> {
    let mut nodes: Vec<Node> = coords.iter().map(Node::new).collect();
    nodes.push(goal.clone());

    let mut open_set = BinaryHeap::new();
    open_set.push(Reverse(start.clone()));

    let mut came_from = HashMap::new();

    while let Some(Reverse(curr)) = open_set.pop() {
        if curr.coord == goal.coord {
            return Some(recon_path(
                came_from,
                curr.coord.clone(),
                start.coord.clone(),
            ));
        }

        // get neighbors
        let neighbors = nodes.iter_mut().filter(|n| {
            let x_dist = curr.coord.x.abs_diff(n.coord.x);
            let y_dist = curr.coord.y.abs_diff(n.coord.y);
            x_dist <= 1 && y_dist <= 1 && !(x_dist == 0 && y_dist == 0)
        });

        for neighbor in neighbors {
            let tentative_g = curr.g + d(curr.coord, neighbor.coord);
            if tentative_g < neighbor.g {
                // this path to neighbor is better than any previous
                came_from.insert(neighbor.coord.clone(), curr.coord.clone());

                neighbor.g = tentative_g;
                if neighbor.h.is_none() {
                    neighbor.h = Some(h(neighbor.coord, goal.coord));
                }
                neighbor.f = tentative_g + neighbor.h.unwrap();

                if !open_set.iter().any(|Reverse(n)| n.coord == neighbor.coord) {
                    open_set.push(Reverse(neighbor.clone()));
                }
            }
        }
    }
    None
}

fn recon_path(came_from: HashMap<Coord, Coord>, mut curr: Coord, start: Coord) -> Vec<Coord> {
    let mut path = vec![curr.clone()];
    while let Some(prev) = came_from.get(&curr) {
        curr = prev.clone();
        path.insert(0, curr.clone());
        if *prev == start {
            break;
        }
    }
    path
}
